import React  from 'react';
import img1 from '../../assets/1.png';
import img2 from '../../assets/2.png';
import img3 from '../../assets/3.png';
import img4 from '../../assets/4.png';
import img5 from '../../assets/5.png';
import img6 from '../../assets/6.png';
import img7 from '../../assets/7.png';
import img8 from '../../assets/8.png';
import img9 from '../../assets/9.png';
import img10 from '../../assets/10.png';

import './styles.css'

const images = [img1, img2, img3, img4, img5, img6, img7, img8, img9, img10];


const Card = ({index}) => {
  return (
    <div className="card">
      <div className="inner">
      <div className="back">
        <img alt="" src={images[index]} />
      </div>
      <div className="front">
       Avengers Team &trade;
       <br />
       <p className="number">
       {index + 1}
       </p>
      </div>
      </div>
    </div>
  )
}

export default Card;