import './App.css';
import Card from './components/Card'

function App() {
  return (
    <div className="App">
      {Array(10).fill('').map((v, i) => {
        return <Card key={i} index={i} />
      })}
    </div>
  );
}

export default App;
